Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# A simple point holder for four different points to create a rectangle.
# Can be used as a construction piece for example for the cube.
# The triangleIndices are defined automatically.

function Set-Window {

    [system.windows.Window] $window = [System.Windows.Markup.XamlReader]::Parse(@'
    <Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="Simple Rect"
    x:Name="MainWindow"
    Height="256" Width="256">
    <Grid>
        <Viewport3D Grid.Row="0" Grid.Column="0" Name="MainViewport"/>
    </Grid>
    </Window>
'@)
    return $window
}

function Set-Camera {
    $PerspectiveCamera = [PerspectiveCamera]::new()
    $PerspectiveCamera.FieldOfView = 90
    $PerspectiveCamera.Position = [Point3D]::new(0,0,5)
    $PerspectiveCamera.LookDirection = [Vector3D]::new(0,0,-5)

    return $perspectiveCamera
}

# Setting up the lights.
Function Set-Lights {
    Param($lightgroup)

    #ambientLight = [AmbientLight]::new('white')
    $directionalLight = [DirectionalLight]::new('white', [Vector3D]::new(-1,-1,-1))
    #Model3Dgroup.Children.Add($ambientLight)
    $lightgroup.Children.Add($directionalLight)
}

Function Draw-Rect {
    Param ([Model3DGroup]$modelGroup)

    ## Create the GeometryModel object: ##
    [GeometryModel3D]$rectModel = [GeometryModel3D]::new();

    ## Create the meshGeometry object: ##
    [MeshGeometry3D]$mesh = [MeshGeometry3D]::new();

    ## Define the vertices and add them to the mesh: ##
    # Bottom left.
    $mesh.Positions.Add([Point3D]::new(0,0,0));
    # Top left.
    $mesh.Positions.Add([Point3D]::new(0,1,0));
    # Top right.
    $mesh.Positions.Add([Point3D]::new(1,1,0));
    # Bottom right. 
    $mesh.Positions.Add([Point3D]::new(1,0,0));

    ## Define the indices, which determine how the triangles are drawn. ##
    # The order is predetermined here. We draw counter clockwise!
    [System.Collections.Generic.List[int32]]$triangleIndices = @(0,2,1, 3,2,0);
    $mesh.TriangleIndices = $triangleIndices;

    ## Define the texture coordinates. ##
    $mesh.TextureCoordinates.Add([Point]::new(0,1));
    $mesh.TextureCoordinates.Add([Point]::new(0,0));
    $mesh.TextureCoordinates.Add([Point]::new(1,0));
    $mesh.TextureCoordinates.Add([Point]::new(1,1));

    ## Add the MeshGeometry to the Geometry Model.
    $rectModel.Geometry = $mesh

    ## Define the image brush and create the material. Add the material to the GeometryModel.
    [string]$textureDir =  Join-Path -Path $PSScriptRoot -childPath .\textures
    [ImageBrush]$brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new("$textureDir\checker.jpg")));
    $rectModel.material = [DiffuseMaterial]::new($brush);
    $rectModel.BackMaterial = [DiffuseMaterial]::new($brush);

    ## Add the Geometry Model to the model group for rendering. ##
    $modelGroup.Children.Add($rectModel);
}

function Initialize-Scene {

    $modelVisual = [ModelVisual3D]::new()
    $Model3Dgroup = [Model3DGroup]::new()
    $lightVisual = [ModelVisual3D]::new()
    $lightGroup = [Model3DGroup]::new()

    $mainWindow = Set-Window
    
    # Construct the scene.    
    Set-Lights -lightgroup $lightGroup

    # Draw the RECT.
    Draw-Rect -ModelGroup $model3DGroup

    # Run this when the window is loaded.
    $mainWindow.Add_Loaded({
        $mainWindow.Content.Background = 'white'
        $MainViewPort = $mainWindow.FindName('MainViewport')
        $MainViewport.Camera = Set-Camera
        $modelVisual.Content = $Model3Dgroup
        $lightVisual.Content = $lightGroup
        $MainViewport.Children.Add($modelVisual)
        $MainViewPort.Children.Add($lightVisual)
    })

    $mainWindow.ShowDialog() | Out-Null
}

Initialize-Scene