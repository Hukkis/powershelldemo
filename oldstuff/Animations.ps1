# Old goofy test animation code stored here for future reference.

# # Placeholder.
# function Initialize-Animations {
#     Param( $Model3Dgroup )

#     foreach ($cube in $Model3Dgroup.Children) {

#         # Get the center origin point of the object in question.
#         $origin = "$(($cube.Bounds.Location.X + ([double]$cube.Bounds.SizeX / 2)),(($cube.Bounds.Location.Y) + (([double]$cube.Bounds.SizeY) / 2)),($cube.Bounds.Location.Z + ([double]$cube.Bounds.SizeZ / 2)))"

#         # From 0, To 360, timespan 3000ms. Change from value 0 -> 360 in the span of 3 seconds.
#         $cubeAnimation1 = [DoubleAnimation]::new(0, 360, (New-Object TimeSpan(0,0,0,0, 3000)))
#         # Start the animation at time 0.
#         $cubeAnimation1.BeginTime = (New-Object TimeSpan(0,0,0,0,0))
#         # Rotation. Rotate by vector x,y,z.
#         $rotation1 = [AxisAngleRotation3D]::new([Vector3D]::new(0,1,0), 0.0);
#         # Create a rotation transform, and define the rotation and origin points for it.
#         $rotateTransform1 = [RotateTransform3D]::new($rotation1, $origin)


#         # From 0, To 360, timespan 3000ms. Change from value 0 -> 360 in the span of 3 seconds.
#         $cubeAnimation2 = [DoubleAnimation]::new(0, 360, (New-Object TimeSpan(0,0,0,0, 3000)))
#         # Start the animation at time 0.
#         $cubeAnimation2.BeginTime = (New-Object TimeSpan(0,0,0,0,3000))
#         # Rotation. Rotate by vector x,y,z.
#         $rotation2 = [AxisAngleRotation3D]::new([Vector3D]::new(1,0,0), 0.0);
#         # Create a rotation transform, and define the rotation and origin points for it.
#         $rotateTransform2 = [RotateTransform3D]::new($rotation2, $origin)

#         # Add the rotation to a transformGroup.
#         $transformGroup = [Transform3DGroup]::new()
#         $transformGroup.Children.Add($rotateTransform1)
#         $transformGroup.Children.Add($rotateTransform2)

#         $cube.transform = $transformGroup
#         $rotateTransform1.rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $cubeAnimation1)
#         $rotateTransform2.rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $cubeAnimation2)
#     }  
# }