Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5


Class EquilateralTriangle {

    [MeshGeometry3D]$meshGeometry
    [GeometryModel3D]$geometryModel

    # Changeable values.
    [Point3D]$origin # Left corner.
    [double]$width
    [double]$height
    $color = '#c909de'
    

    # Constructor.
    EquilateralTriangle ([Point3D]$origin, [double]$width, [double]$height) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height

        $this.Draw();

    }

    EquilateralTriangle ([Point3D]$origin, [double]$width, [double]$height, [string]$color) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        if(($color.startswith('#')) -and ($color.length -eq 7)) { $this.color = $color }

        $this.Draw();

    }

    [GeometryModel3D]GetGeometryModel(){
        return $this.geometryModel
    }

    [void]Draw () {

        $this.geometryModel = [GeometryModel3D]::new()
        $this.meshGeometry = [MeshGeometry3D]::new()

        $vertices = [Point3DCollection]::new()
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)) # Index 0, Left corner.
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z)) # Index 1
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + 0, $this.origin.Z + $this.width)) # Index 2
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + $this.height, $this.origin.Z + ($this.width / 2))) # Index 3, Top point.
        
        $this.meshGeometry.Positions = $vertices

        # The indices of the vertices that define the triangles. The indices represent the corners of each triangle.
        # Three indices form a single triangle. In the case of a cube, we define two triangles per face == 12 triangles.
        $indices = @(0,1,3, 2,0,3, 1,2,3, 0,2,1)
                                                            
        $triangleIndices = [Int32Collection]::new()
        foreach ($index in $indices) {
            $triangleIndices.Add($index)
        }

        # The mesh is formed from the triangleIndices (triangle corners).
        $this.meshGeometry.TriangleIndices = $triangleIndices
    
        # Define the cube material.
        $material = New-Object System.Windows.Media.Media3D.DiffuseMaterial -Property @{Brush = $this.color}
    
        # Set the cube geometry model.
        $this.geometryModel.Geometry = $this.meshGeometry
        $this.geometryModel.Material = $material
        $this.geometryModel.BackMaterial = $material
    }
}
