# For future reference.

Function Initialize-Lights {
    Param([Model3DGroup]$lightgroup)

    # Singular ambient light, that illuminates the whole scene.
    #$lightgroup.Children.Add([AmbientLight]::new('gray'))

    $lightGroup.Children.Add([PointLight]::new('orange', [Point3D]::new(0,20,-8)))

    # for testing..
    # $spotLight = [SpotLight]::new(
    #         'white',
    #         [Point3D]::new(
    #             -5,
    #             15,
    #             0
    #         ),
    #         [Vector3D]::new(0,-1,0),
    #         100,
    #         90
    #     )
    # $lightGroup.Children.Add($spotLight)

  #  SpotLight(Color, Point3D, Vector3D, Double, Double)

    # Directional lights, that illuminate towards the given vector.
    # To illuminate most of the scene, we need two of these.
    #$lightgroup.Children.Add([DirectionalLight]::new('gray', [Vector3D]::new(0,-1,-1)))
    # $lightgroup.Children.Add([DirectionalLight]::new('white', [Vector3D]::new(1,1,-1)))

}