Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5


# Notes:
# Window has viewport
# Viewport has a camera and a ModelVisual
# ModelVisual has a Model3DGroup
# Model3DGroup has lights and models (GeometryModel3D)
# GeometryModel3D has Geometry (MeshGeometry3D)
# MeshGeometry3D has TriangleIndices & Positions (3DPoints)


function Set-Window {

    [system.windows.Window] $window = [System.Windows.Markup.XamlReader]::Parse(@'
    <Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="PowerShell Demo"
    x:Name="MainWindow"
    Height="1024" Width="1024">
    <Grid>
        <Viewport3D Grid.Row="0" Grid.Column="0" Name="MainViewport"/>
    </Grid>
    </Window>
'@)
    return $window
}


function Set-Camera {
    $PerspectiveCamera = [PerspectiveCamera]::new()
    $PerspectiveCamera.FieldOfView = 90
    $PerspectiveCamera.Position = [Point3D]::new(6,5,4)
    $PerspectiveCamera.LookDirection = [Vector3D]::new(-6,-5,-4)

    return $perspectiveCamera
}

# Setting up the lights.
Function Set-Lights {
    Param($lightgroup)

    #ambientLight = [AmbientLight]::new('white')
    $directionalLight = [DirectionalLight]::new('white', [Vector3D]::new(-1,-1,-1))
    #Model3Dgroup.Children.Add($ambientLight)
    $lightgroup.Children.Add($directionalLight)
}

# Setting up the cube model.
function Set-Cube {
    Param (
        $modelGroup,
        $posX = 0,
        $posY = 0,
        $posZ = 0,
        $color = '#000000'
    )

    $cubeModel = [GeometryModel3D]::new()

    # Define the cube mesh.
    $cubeMesh = [MeshGeometry3D]::new()
    
    $positionCollection = [Point3DCollection]::new()
    $positionCollection.Add([Point3D]::new($posX + 0,$posY + 0,$posZ + 0)) # Index 0
    $positionCollection.Add([Point3D]::new($posX + 1,$posY + 0,$posZ + 0)) # Index 1
    $positionCollection.Add([Point3D]::new($posX + 0,$posY + 1,$posZ + 0)) # Index 2
    $positionCollection.Add([Point3D]::new($posX + 1,$posY + 1,$posZ + 0)) # Index 3
    $positionCollection.Add([Point3D]::new($posX + 0,$posY + 0,$posZ + 1)) # Index 4
    $positionCollection.Add([Point3D]::new($posX + 1,$posY + 0,$posZ + 1)) # Index 5
    $positionCollection.Add([Point3D]::new($posX + 0,$posY + 1,$posZ + 1)) # Index 6
    $positionCollection.Add([Point3D]::new($posX + 1,$posY + 1,$posZ + 1)) # Index 7
    $cubeMesh.Positions = $positionCollection

    # The indices of the vertices that define the triangles. The indices represent the corners of each triangle.
    # Three indices form a single triangle. In the case of a cube, we define two triangles per face == 12 triangles.
    $indices = @(2,3,1, 2,1,0, 7,1,3, 7,5,1, 6,5,7, 6,4,5, 6,0,4, 6,2,0, 2,7,3, 2,6,7, 0,1,5, 0,5,4)
                                                           
    $triangleIndices = [Int32Collection]::new()
    foreach ($index in $indices) {
        $triangleIndices.Add($index)
    }

    # The mesh is formed from the triangleIndices (triangle corners).
    $cubeMesh.TriangleIndices = $triangleIndices

    # Define the cube material.
    $cubeMaterial = New-Object System.Windows.Media.Media3D.DiffuseMaterial -Property @{Brush = $color}

    # Set the cube geometry model.
    $cubeModel.Geometry = $cubeMesh
    $cubeModel.Material = $cubeMaterial
    $cubeModel.BackMaterial = $cubeMaterial
    
    $modelGroup.Children.add($cubeModel)
}

function Set-Triangle {
    Param (
        $modelGroup,
        $posX = 0,
        $posY = 0,
        $posZ = 0,
        $color = '#000000'
    )

    $cubeModel = [GeometryModel3D]::new()

    # Define the cube mesh.
    $cubeMesh = [MeshGeometry3D]::new()
    
    $positionCollection = [Point3DCollection]::new()
    $positionCollection.Add([Point3D]::new($posX + 0,$posY + 0,$posZ + 0)) # Index 0
    $positionCollection.Add([Point3D]::new($posX + 2,$posY + 0,$posZ + 0)) # Index 1
    $positionCollection.Add([Point3D]::new($posX + 1,$posY + 0,$posZ + 2)) # Index 2
    $positionCollection.Add([Point3D]::new($posX + 1,$posY + 2,$posZ + 1)) # Index 3
    $cubeMesh.Positions = $positionCollection

    # The indices of the vertices that define the triangles. The indices represent the corners of each triangle.
    # Three indices form a single triangle. In the case of a cube, we define two triangles per face == 12 triangles.
    $indices = @(0,1,3, 2,0,3, 1,2,3, 0,2,1)
                                                           
    $triangleIndices = [Int32Collection]::new()
    foreach ($index in $indices) {
        $triangleIndices.Add($index)
    }

    # The mesh is formed from the triangleIndices (triangle corners).
    $cubeMesh.TriangleIndices = $triangleIndices

    # Define the cube material.
    $cubeMaterial = New-Object System.Windows.Media.Media3D.DiffuseMaterial -Property @{Brush = $color}

    # Set the cube geometry model.
    $cubeModel.Geometry = $cubeMesh
    $cubeModel.Material = $cubeMaterial
    $cubeModel.BackMaterial = $cubeMaterial

    $modelGroup.Children.add($cubeModel)

}



function Initialize-Animations {
    Param( $Model3Dgroup )

    Write-Host $Model3Dgroup
    foreach ($cube in $Model3Dgroup.Children) {

        # Get the center origin point of the object in question.
        $origin = "$(($cube.Bounds.Location.X + ([double]$cube.Bounds.SizeX / 2)),(($cube.Bounds.Location.Y) + (([double]$cube.Bounds.SizeY) / 2)),($cube.Bounds.Location.Z + ([double]$cube.Bounds.SizeZ / 2)))"

        # From 0, To 360, timespan 3000ms. Change from value 0 -> 360 in the span of 3 seconds.
        $cubeAnimation1 = [DoubleAnimation]::new(0, 360, (New-Object TimeSpan(0,0,0,0, 3000)))
        # Start the animation at time 0.
        $cubeAnimation1.BeginTime = (New-Object TimeSpan(0,0,0,0,0))
        # Rotation. Rotate by vector x,y,z.
        $rotation1 = [AxisAngleRotation3D]::new([Vector3D]::new(0,1,0), 0.0);
        # Create a rotation transform, and define the rotation and origin points for it.
        $rotateTransform1 = [RotateTransform3D]::new($rotation1, $origin)


        # From 0, To 360, timespan 3000ms. Change from value 0 -> 360 in the span of 3 seconds.
        $cubeAnimation2 = [DoubleAnimation]::new(0, 360, (New-Object TimeSpan(0,0,0,0, 3000)))
        # Start the animation at time 0.
        $cubeAnimation2.BeginTime = (New-Object TimeSpan(0,0,0,0,3000))
        # Rotation. Rotate by vector x,y,z.
        $rotation2 = [AxisAngleRotation3D]::new([Vector3D]::new(1,0,0), 0.0);
        # Create a rotation transform, and define the rotation and origin points for it.
        $rotateTransform2 = [RotateTransform3D]::new($rotation2, $origin)

        # Add the rotation to a transformGroup.
        $transformGroup = [Transform3DGroup]::new()
        $transformGroup.Children.Add($rotateTransform1)
        $transformGroup.Children.Add($rotateTransform2)

        $cube.transform = $transformGroup
        $rotateTransform1.rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $cubeAnimation1)
        $rotateTransform2.rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $cubeAnimation2)
    }
    
}


# Initialize the scene.
# Call the functions that create the window, camera, models etc.
# Render the scene.
function Initialize-Scene {

    # Properties.
    $modelVisual = [ModelVisual3D]::new()
    $Model3Dgroup = [Model3DGroup]::new()
    $lightVisual = [ModelVisual3D]::new()
    $lightGroup = [Model3DGroup]::new()

    $mainWindow = Set-Window
    
    # Construct the scene.    
    Set-Lights -lightgroup $lightGroup

    # Testing with different values.
    Set-Triangle -ModelGroup $model3DGroup -posX 0 -posY 4 -posZ 0 -color '#c909de'
    Set-Cube -ModelGroup $Model3Dgroup -posX 2 -posY 2 -posZ 2 -color '#de0909'
    Set-Cube -ModelGroup $Model3Dgroup -posX 2 -posY 2 -posZ 0 -color '#c909de'
    Set-Cube -ModelGroup $Model3Dgroup -posX 0 -posY 2 -posZ 2 -color '#178fc2'
    Set-Cube -ModelGroup $Model3Dgroup -posX 0 -posY 2 -posZ 0 -color '#17c22e'

    # Run this when the window is loaded.
    $mainWindow.Add_Loaded({
        $mainWindow.Content.Background = 'white'
        $MainViewPort = $mainWindow.FindName('MainViewport')
        $MainViewport.Camera = Set-Camera
        $modelVisual.Content = $Model3Dgroup
        $lightVisual.Content = $lightGroup
        $MainViewport.Children.Add($modelVisual)
        $MainViewPort.Children.Add($lightVisual)
        Initialize-Animations -Model3Dgroup $Model3Dgroup
    })

    $mainWindow.ShowDialog() | Out-Null
}

Initialize-Scene
