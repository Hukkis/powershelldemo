Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# Include classes PowerShell style :D.
# Unfortunately, these have to be source in the right order.
. .\class\Scene\SceneWindow.ps1
. .\class\Scene\Camera.ps1
. .\class\3DObjects\WorldObject3D.ps1
. .\class\3DObjects\cube.ps1
. .\class\3DObjects\Pyramid.ps1
. .\class\3DObjects\TexturedPyramid.ps1
. .\class\3DObjects\Plane.ps1
. .\class\3DObjects\Rectangle3D.ps1
. .\class\3DObjects\RectangleCube.ps1
. .\class\3DObjects\Door.ps1
. .\class\3DObjects\RectangleGrid.ps1
. .\class\3DObjects\Cabin.ps1
. .\class\3DObjects\Cylinder.ps1
. .\class\3DObjects\LightPost.ps1
. .\class\3DObjects\Tree.ps1
. .\class\3DObjects\Forest.ps1

##########
# Notes:
# Window has viewport
# Viewport has a camera and a ModelVisual
# ModelVisual has a Model3DGroup or a Model
# Model3DGroup has lights and models (GeometryModel3D)
# GeometryModel3D has Geometry (MeshGeometry3D)
# MeshGeometry3D has TriangleIndices & Positions (3DPoints)
#########

#################################################################
# Controls implemented here for now.
# This should be a class method in the future [DefaultScene] class.
# Dirty solution for now.
# The TEST controls are very wonky :D
Function Set-CameraControls {
    Param($window, $cameraHolder)

    $window.Add_KeyDown({
        Param ([object]$sender, [System.Windows.Input.KeyEventArgs]$eventArgs)

        $direction = [Vector3D]::new(0,0,0)
        $lookdirection = [Vector3D]::new(0,0,0)

        Switch ($eventArgs.key){
            'w'{
                # Forward.
                $cameraHolder.MoveCamera(
                    [Vector3D]::new(0,0,-1)
                )
                Break;
            }
            's'{
                # Backwards.
                $cameraHolder.MoveCamera(
                    [Vector3D]::new(0,0,1)
                )
                Break;
            }
            'a'{
                # Left.
                $cameraHolder.MoveCamera(
                    [Vector3D]::new(-1,0,0)
                )
                Break;
            }
            'd'{
                # Right.
                $cameraHolder.MoveCamera(
                    [Vector3D]::new(1,0,0)
                )
                Break;
            }
            'e'{
                # Up.
                $cameraHolder.MoveCamera(
                    [Vector3D]::new(0,1,0)
                )
                Break;
            }
            'q'{
                # Down.
                $cameraHolder.MoveCamera(
                    [Vector3D]::new(0,-1,0)
                )
                Break;
            }
            'Up'{
                # Upwards.
                $cameraHolder.RotateCamera(
                    [Vector3D]::new(0,1,0)
                )
                Break;
            }
            'Down'{
                # Downwards.
                $cameraHolder.RotateCamera(
                    [Vector3D]::new(0,-1,0)
                )
                Break;
            }
            'Left'{
                # Towards left.
                $cameraHolder.RotateCamera(
                    [Vector3D]::new(-1,0,0)
                )
                Break;
            }
            'Right'{
                # Towards right.
                $cameraHolder.RotateCamera(
                    [Vector3D]::new(1,0,0)
                )
                Break;
            }
            default{
                break;
            }
        }
    })
}

# Setting up the lights.
# Adds the light effects to the given Model3DGroup.
Function Initialize-Lights {
    Param([Model3DGroup]$lightgroup)

    # Singular ambient light, that illuminates the whole scene.
    $lightgroup.Children.Add([AmbientLight]::new('gray'))

    # Singular "sunlike" point light that somewhat illuminates the whole scene.
    # $pointLight = [PointLight]::new('orange', [Point3D]::new(0,20,-8))
    # $pointLight.Range = 10000;
    # $lightGroup.Children.Add($pointLight)


    # Directional lights, that illuminate towards the given vector.
    # To illuminate most of the scene, we need two of these.
    # $lightgroup.Children.Add([DirectionalLight]::new('white', [Vector3D]::new(0,-1,-1)))
    # $lightgroup.Children.Add([DirectionalLight]::new('white', [Vector3D]::new(1,1,-1)))

}

function Initialize-Models {
    Param([Model3DGroup]$modelGroup)

    $texturePath = Join-Path -Path $PSScriptRoot -childPath .\textures

    # Construct the scene.  
    # Create the ground grid.
    $ground = [RectangleGrid]::new(
        [Point3D]::new(-150,0,100),
        300,
        120,
        10,
        10
    )
    $ground.AddTexture("$texturePath\seamlessgrass.jpg")
    $modelGroup.Children.add($ground.GetGeometryModel())

    $road = [RectangleGrid]::new(
        [Point3D]::new(-150, .1, 20),
        300,
        20,
        10,
        1
    )
    $road.AddTexture("$texturePath\cobble.jpg")
    $modelGroup.Children.add($road.GetGeometryModel())

    # Create the sea element.
    $sea = [RectangleGrid]::new(
        [Point3D]::new(-150,0,-20),
        300,
        40,
        1,
        1
    )
    $sea.AddTexture("$texturePath\sea.jpg")
    $modelGroup.Children.Add($sea.GetGeometryModel())

    # # Create the sky backgrounds.
    [Plane[]]$skyPlanes = @(
        [Plane]::new(
            [Point3D]::new(-150,-0.1,-60),
            [Point3D]::new(-150,100,-30),
            [Point3D]::new(150,100,-30),
            [Point3D]::new(150,-0.1,-60)
        ),
        [Plane]::new(
            [Point3D]::new(-150,-0.1,100),
            [Point3D]::new(-120,100,100),
            [Point3D]::new(-120,100,-60),
            [Point3D]::new(-150,-0.1,-60)
        ),
        [Plane]::new(
            [Point3D]::new(150,-0.1,100),
            [Point3D]::new(120,100,100),
            [Point3D]::new(120,100,-60),
            [Point3D]::new(150,-0.1,-60)
        )
    )

    foreach ($plane in $skyPlanes) {
        $plane.AddTexture("$texturePath\horizon.jpg")
        $modelGroup.Children.add($plane.GetGeometryModel())
    }

    # Create the skybox.
    # $skyBox = [RectangleCube]::new([Point3D]::new(-150,-100,100),300,200,300,'#234234')
    # $skyBox.AddMappedTexture("$texturePath\maps.jpg")
    # $modelGroup.Children.add($skyBox.GetGeometryModel())

    # Pumpkins.
    $pumpkin_01 = [RectangleCube]::new([Point3D]::new(-3,0,0),2,2,2,'#234234')
    $pumpkin_01.AddTextureOnAllFaces("$texturePath\pumpkin.png")
    $modelGroup.Children.add($pumpkin_01.GetGeometryModel())

    $pumpkin_02 = [RectangleCube]::new([Point3D]::new(21,0,0),2,2,2,'#234234')
    $pumpkin_02.AddTextureOnAllFaces("$texturePath\pumpkin.png")
    $modelGroup.Children.add($pumpkin_02.GetGeometryModel())

    # Dice. Just testing, move these.
    [RectangleCube[]]$dice = @(
        [RectangleCube]::new([Point3D]::new(12,0.3,4),.5,.5,.5,'#234234'),
        [RectangleCube]::new([Point3D]::new(13,0.3,4),.5,.5,.5,'#234234'),
        [RectangleCube]::new([Point3D]::new(14,0.3,4),.5,.5,.5,'#234234'),
        [RectangleCube]::new([Point3D]::new(12,0.3,6),.5,.5,.5,'#234234'),
        [RectangleCube]::new([Point3D]::new(13,0.3,6),.5,.5,.5,'#234234'),
        [RectangleCube]::new([Point3D]::new(14,0.3,6),.5,.5,.5,'#234234')
    )

    foreach($die in $dice) {
        $die.AddMappedTexture("$texturePath\dice_unwrap.png")
        $modelGroup.Children.Add($die.GetGeometryModel())
        $die.Rotate(
            [Vector3D]::new(1,1,1),
            18000,
            100000
        )
    }

    # Forests.

    [Forest]$forestB = [Forest]::new(
        [Point3D]::new(-100,0,-11),
        100,
        20,
        "$texturePath\tree.jpg", 
        "$texturePath\leaves.png"
    )
    $forestB.AddToModelGroup($modelGroup);

    [Forest]$forestL = [Forest]::new(
        [Point3D]::new(-120,0,25),
        100,
        100,
        "$texturePath\tree.jpg", 
        "$texturePath\leaves.png"
    )
    $forestL.AddToModelGroup($modelGroup);

    # Cabin.
    $cabin = [Cabin]::new(
        [Point3D]::new(0,0,0),
        20, 
        10, 
        20,
        "$texturePath\wall.jpg", 
        "$texturePath\plank.jpg", 
        "$texturePath\plank.jpg",
        "$texturePath\door.jpg"
    )
    $cabin.AddToModelGroup($modelGroup)
    $cabin.OpenDoor(0)

    $cabin2 = [Cabin]::new(
        [Point3D]::new(27.5,0,0),
        15, 
        8, 
        15,
        "$texturePath\logs.jpg", 
        "$texturePath\plank.jpg", 
        "$texturePath\rooftilesrot.jpg",
        "$texturePath\door.jpg"
    )
    $cabin2.AddToModelGroup($modelGroup)

    # Lightposts.
    [LightPost[]]$lightPosts = @(
        [LightPost]::new([Point3D]::new(-5,0,0)),
        [LightPost]::new([Point3D]::new(-25,0,0)),
        [LightPost]::new([Point3D]::new(-45,0,0)),
        [LightPost]::new([Point3D]::new(25,0,0)),
        [LightPost]::new([Point3D]::new(45,0,0)),
        [LightPost]::new([Point3D]::new(65,0,0))
    )

    foreach ($post in $lightPosts) {
        $modelGroup.Children.Add($post.GetGeometryModel())
        $modelGroup.Children.add($post.GetLightObject())
    }

    # Pyramids for testing.
    $Pyramid = [Pyramid]::new(
        [Point3D]::new(50,0,-15),
        10,
        10,
        "#fff444"
    )   
    $Pyramid.AddTexturedMaterial("$texturePath\concrete.jpg")  
    $modelGroup.Children.Add($Pyramid.GetGeometryModel())
    $Pyramid.Rotate(
        [Vector3D]::new(0,1,0),
        450,
        30000
    )

    $texturedPyramid = [TexturedPyramid]::new(
        [Point3D]::new(75, 0, -15),
        10,
        10,
        "$texturePath\plank.jpg"
    )
    $modelGroup.Children.Add($texturedPyramid.GetGeometryModel())
    $texturedPyramid.Rotate(
        [Vector3D]::new(0,1,0),
        450,
        30000
    )
}

# Initialize the scene.
# Call the functions that create the window, camera, models etc.
# Render the scene.
# TODO: move these to the DefaultScene class.
function Initialize-Scene {

    # Properties.
    # Add one Model3DGroup to work as a "base container", aka the world.
    # Add different Model3DGroups to that base container. 
    $modelVisual = [ModelVisual3D]::new()
    $lightVisual = [ModelVisual3D]::new()
    $lightGroup = [Model3DGroup]::new()
    $modelGroup = [Model3DGroup]::new()

    # Add all the models to the modelGroup.
    Initialize-Models -modelGroup $modelGroup
    Initialize-Lights -lightgroup $lightGroup

    # Using script / global variables is dirty, but this will be refactored when the Scene class is implemented.
    $Script:sceneWindow = [SceneWindow]::new()
    $Script:mainWindow = $sceneWindow.GetWindow()
    
    $global:cameraHolder = [CameraHolder]::new(
        [Point3D]::new(0,15,70),
        [Vector3D]::new(0, -5, -12),
        [Vector3D]::new(0.0, 1.0, 0.0)
        )
        $global:camera = $cameraHolder.GetCamera()
        
        Set-CameraControls -window $mainWindow -cameraHolder $cameraHolder
    
    # Run this when the window is loaded.
    $mainWindow.Add_Loaded({
        $mainWindow.Content.Background = 'white'
        $MainViewPort = $mainWindow.FindName('MainViewport')
        $MainViewport.Camera = $camera
        $modelVisual.Content = $modelGroup
        $lightVisual.Content = $lightGroup
        $MainViewport.Children.Add($modelVisual)
        $MainViewPort.Children.Add($lightVisual)
    })

    $mainWindow.ShowDialog() | Out-Null
}

# Run the show.
Initialize-Scene