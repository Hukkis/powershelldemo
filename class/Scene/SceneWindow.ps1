Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

Class SceneWindow {

    [float]$windowWidth = 1024
    [float]$windowHeight = 1024
    [string]$sceneName = "PowerShell Demo"
    [system.windows.Window]$window


    SceneWindow () { $this.SetWindow() }

    SceneWindow ([float]$width, [float]$height) {
        $this.windowWidth = $width
        $this.windowHeight = $height
        $this.SetWindow()
    }

    SceneWindow ([float]$width, [float]$height, [string]$name) {
        $this.windowHeight = $height
        $this.windowWidth = $width
        $this.sceneName = $name
        $this.SetWindow()
    }

    # Ugly indentation due to the herestring.
    [void]SetWindow() {

        $this.window = [System.Windows.Markup.XamlReader]::Parse(@"
        <Window
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        Title="$($this.sceneName)"
        x:Name="MainWindow"
        Height="$($this.windowHeight)" Width="$($this.windowWidth)">
        <Grid>
            <Viewport3D Grid.Row="0" Grid.Column="0" Name="MainViewport"/>
        </Grid>
        </Window>
"@)

    }

    [System.Windows.Window]GetWindow () {
        return $this.window;
    }
}