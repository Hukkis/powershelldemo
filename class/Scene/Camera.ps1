Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

class CameraHolder {

    [PerspectiveCamera]$Camera
    [Point3D]$InitialPosition
    [float]$FOV = 90


    CameraHolder ([Point3D]$initialPosition, [Vector3D]$lookDirection, [Vector3D]$upDirection) {

        $this.Camera = [PerspectiveCamera]::new()
        $this.InitialPosition = $initialPosition

        $this.Camera.Position = $initialPosition
        $this.Camera.LookDirection = $lookDirection
        $this.Camera.UpDirection = $upDirection
        $this.Camera.FieldOfView = $this.FOV

    }

    [PerspectiveCamera]GetCamera () {
        return $this.Camera
    }

    [Void]MoveCamera([Vector3D]$direction) {

        $startPosition = $this.Camera.Position
        [Point3D]$endPosition = [Point3D]::new(0,0,0)
        $endPosition = [Point3D]::Add($startPosition,$direction)
        $this.Camera.Position = $endPosition 
    
    }

    [Void]RotateCamera([Vector3D]$lookDirection) {
    
        $startLookAt = $this.Camera.LookDirection
        [Vector3D]$lookAt = [Vector3D]::new(0,0,0)
        $lookAt = $startLookAt + $lookDirection
        $this.Camera.LookDirection = $lookAt
    }



}