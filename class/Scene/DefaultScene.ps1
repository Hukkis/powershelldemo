Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# A placeholder. Not yet implemented.
# The idea is to build a defaultScene class, that has all the needed properties to render a 3D world in a window.
# We would then have subclasses derived from the [DefaultScene], that would hold the models etc.
# This way it would be possible to load multiple different scenes.

Class DefaultScene {

    [system.windows.Window]$mainWindow
    [PerspectiveCamera]$mainCamera


    DefaultScene () {
        $this.SetWindow()
    }

    DefaultScene () {

    }

    [void]SetWindow () {

    }

    [void]SetCamera () {

    }

}