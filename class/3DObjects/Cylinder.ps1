Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# Creates a Cylinder object.
# The calculations in the Draw method were borrowed from the following article
# http://csharphelper.com/blog/2015/04/draw-smooth-cylinders-using-wpf-and-c/ .

Class Cylinder : WorldObject3D {

    [Point3D]$origin
    [Vector3D]$axis
    [Double]$radius
    [int]$sides
    [string]$color = '#b0b0b0'

    Cylinder ([Point3D]$origin, [Vector3D]$axis, [double]$radius, [int]$sides) {

        $this.origin = $origin;
        $this.axis = $axis;
        $this.radius = $radius;
        $this.sides = $sides;

        $this.Draw();
    }

    [void]Draw() {

        # Define two vectors perpendicular to the axis, $v1 and $v2.
        if(($this.axis.Z -lt -0.01) -or ($this.axis.Z -gt 0.01)) {
            [Vector3D]$v1 = [Vector3D]::new($this.axis.Z, $this.axis.Z, (-$this.axis.X - $this.axis.Y))
        } else {
            [Vector3D]$v1 = [Vector3D]::new((-$this.axis.Y - $this.axis.Z), $this.axis.X, $this.axis.X)
        }

        [Vector3D]$v2 = [Vector3D]::CrossProduct($v1, $this.axis);
        
        # Make the vectors have length equal to radius.
        $v1 *= ($this.radius / $v1.Length);
        $v2 *= ($this.radius / $v2.Length);
        # Calculate the theta delta.
        [double]$dtheta = (2 * [Math]::PI) / $this.sides;

        ## Create the top cap.
        # Create the end point.
        [int]$pt0 = $this.meshGeometry.Positions.Count;
        $this.meshGeometry.Positions.Add($this.origin);

        # Make the top points.
        [double]$theta = 0;
        for ($i = 0; $i -lt $this.sides; $i++)
        {
            $this.meshGeometry.Positions.Add(
                $this.origin +
                [Math]::Cos($theta) * $v1 +
                [Math]::Sin($theta) * $v2
            );
        }

        ## Make the top triangles (indices).
        # Index of the last point.
        [int]$pt1 = $this.meshGeometry.Positions.Count - 1;
        # Index of first point.
        [int]$pt2 = $pt0 + 1

        for ([int]$i = 0; $i -lt $this.sides; $i++) {
            $this.meshGeometry.TriangleIndices.Add($pt0);
            $this.meshGeometry.TriangleIndices.Add($pt1);
            $this.meshGeometry.TriangleIndices.Add($pt2);
            $pt1 = ($pt2++);
        }

        ## Make the bottom cap.
        # The end point -> Index of end_point2.
        $pt0 = $this.meshGeometry.Positions.Count;
        [Point3D]$endPoint2 = $this.origin + $this.axis;
        $this.meshGeometry.Positions.Add($endPoint2);

        # The bottom points.
        $theta = 0;
        for ([int]$i = 0; $i -lt $this.sides; $i++){
            $this.meshGeometry.Positions.Add(
                $endPoint2 + 
                [Math]::Cos($theta) * $v1 +
                [Math]::Sin($theta) * $v2
            );
            $theta += $dtheta;
        }

        ## The bottom triangles.
        $theta = 0;
        # Index of last point.
        $pt1 = ($this.meshGeometry.Positions.Count -1);
        # Index of first point.
        $pt2 = ($pt0 + 1);

        for ([int]$i = 0; $i -lt $this.sides; $i++) {
            $this.meshGeometry.TriangleIndices.Add($this.sides + 1);
            $this.meshGeometry.TriangleIndices.Add($pt2);
            $this.meshGeometry.TriangleIndices.Add($pt1);
            $pt1 = $pt2++;
        }

        ## Make the sides.
        # Define the side points.
        [int]$firstPoint = $this.meshGeometry.Positions.Count;
        $theta = 0;
        for([int]$i = 0; $i -lt $this.sides; $i++ ) {
            [Point3D]$p1 = $this.origin + [Math]::Cos($theta) * $v1 + [Math]::Sin($theta) * $v2;
            $this.meshGeometry.Positions.Add($p1);
            [Point3D]$p2 = $p1 + $this.axis;
            $this.meshGeometry.Positions.Add($p2);
            $theta += $dtheta;
        }

        # Make the side triangles.
        $pt1 = $this.meshGeometry.Positions.Count -2;
        $pt2 = $pt1 + 1;
        [int]$pt3 = $firstPoint;
        [int]$pt4 = $pt3 + 1;

        for ([int]$i = 0; $i -lt $this.sides; $i++) {
            $this.meshGeometry.TriangleIndices.Add($pt1)
            $this.meshGeometry.TriangleIndices.Add($pt2)
            $this.meshGeometry.TriangleIndices.Add($pt4)

            $this.meshGeometry.TriangleIndices.Add($pt1)
            $this.meshGeometry.TriangleIndices.Add($pt4)
            $this.meshGeometry.TriangleIndices.Add($pt3)

            $pt1 = $pt3;
            $pt3 += 2;
            $pt2 = $pt4;
            $pt4 += 2;
        }

        $this.geometryModel.Geometry = $this.meshGeometry;
        ([WorldObject3D]$this).AddColoredMaterial($this.color);

    }
}

#     // Make the side triangles.
#     pt1 = mesh.Positions.Count - 2;
#     pt2 = pt1 + 1;
#     int pt3 = first_side_point;
#     int pt4 = pt3 + 1;
#     for (int i = 0; i < num_sides; i++)
#     {
#         mesh.TriangleIndices.Add(pt1);
#         mesh.TriangleIndices.Add(pt2);
#         mesh.TriangleIndices.Add(pt4);

#         mesh.TriangleIndices.Add(pt1);
#         mesh.TriangleIndices.Add(pt4);
#         mesh.TriangleIndices.Add(pt3);

#         pt1 = pt3;
#         pt3 += 2;
#         pt2 = pt4;
#         pt4 += 2;
#     }
# }


