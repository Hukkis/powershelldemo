Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# The very first practice cube.
Class Cube {

    [MeshGeometry3D]$meshGeometry
    [GeometryModel3D]$geometryModel

    # Changeable values.
    [Point3D]$origin # Left corner.
    [double]$width
    [double]$height
    [double]$depth
    $color = '#c909de'
    

    # Constructor.
    Cube ([Point3D]$origin, [double]$width, [double]$height, [double]$depth) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        $this.depth = $depth

        $this.Draw();
    }

    Cube ([Point3D]$origin, [double]$width, [double]$height, [double]$depth, [string]$color) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        $this.depth = $depth
        if(($color.startswith('#')) -and ($color.length -eq 7)) { $this.color = $color }

        $this.Draw();
    }

    [GeometryModel3D]GetGeometryModel(){
        return $this.geometryModel
    }

    [void]Draw () {

        $this.geometryModel = [GeometryModel3D]::new()
        $this.meshGeometry = [MeshGeometry3D]::new()
        $vertices = [Point3DCollection]::new()

        # Define the corners of the cube in 3D points (vertices).
        # Front.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)) # Index 0
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z)) # Index 1
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z)) # Index 2
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z)) # Index 3

        # Back.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z + $this.depth)) # Index 4
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z + $this.depth)) # Index 5
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z + $this.depth)) # Index 6
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z + $this.depth)) # Index 7

        # Feed the vertex positions to the mesh.
        $this.meshGeometry.Positions = $vertices
    
        # The indices of the vertices that define the triangles. The indices represent the corners of each triangle.
        # Three indices form a single triangle. In the case of a cube, we define two triangles per face == 12 triangles.
        [int[]]$indices = @(2,3,1, 2,1,0, 7,1,3, 7,5,1, 6,5,7, 6,4,5, 6,0,4, 6,2,0, 2,7,3, 2,6,7, 0,1,5, 0,5,4)
    
        $triangleIndices = [Int32Collection]::new()
        foreach ($index in $indices) {
            $triangleIndices.Add($index)
        }

        # The mesh is formed from the triangleIndices (triangle corners).
        $this.meshGeometry.TriangleIndices = $triangleIndices
    
        # Define the cube material.
        $material = New-Object System.Windows.Media.Media3D.DiffuseMaterial -Property @{Brush = $this.color}        
    
        # Set the cube geometry model.
        $this.geometryModel.Geometry = $this.meshGeometry
        $this.geometryModel.Material = $material
        $this.geometryModel.BackMaterial = $material
    }
}

