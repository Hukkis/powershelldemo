Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# A general rectangle formed from four Vertices.
# This is used as a base structure for the basic models.
Class WorldObject3D {

    [MeshGeometry3D]$meshGeometry
    [GeometryModel3D]$geometryModel

    # Constructor.
    # Create the empty objects for the mesh.
    # Maybe something else in the future.
    WorldObject3D () {
        $this.geometryModel = [GeometryModel3D]::new()
        $this.meshGeometry = [MeshGeometry3D]::new()
    }

    [GeometryModel3D]GetGeometryModel() {
        return $this.geometryModel
    }

    [Point3D]GetCenterPoint() {

        $model = $this.geometryModel
        return [Point3D]::new(
            ($model.Bounds.Location.X + ([double]$model.Bounds.SizeX / 2)),
            (( $model.Bounds.Location.Y) + ([double]$model.Bounds.SizeY / 2)),
            ($model.Bounds.Location.Z + ([double]$model.Bounds.SizeZ / 2))
        )
    }

    # Merge another mesh to this object.
    [void]CombineMesh ([MeshGeometry3D]$mesh) {

        # First we add the triangleIndices.
        # If this mesh already contains indices and points, 
        # we need to add the number of points to the current index.        
        foreach ($index in $mesh.triangleIndices) {
            $this.meshGeometry.TriangleIndices.Add($index + $this.meshGeometry.Positions.Count)
        }
 
        # Add the positions next.
        foreach ($vertex in $mesh.positions) {
            $this.meshGeometry.positions.Add($vertex);
        }

        # Lastly add the textureCoordinates.
        # Not sure about this yet.
        foreach ($coord in $mesh.textureCoordinates) {
            $this.meshGeometry.textureCoordinates.Add($coord)
        }
    }

    # This method adds the vetex and index collections to the mesh.
    # The actual calculations for the model in question are done in each subclass.
    [void]Draw () {
        $this.Draw($this.meshGeometry.positions, $this.meshGeometry.triangleIndices)
    }

    [void]Draw ([Point3DCollection]$vertices, [int[]]$indices) {
        # Feed the vertex positions to the mesh.
        $this.meshGeometry.Positions = $vertices

        # Assing the indices to a collection, and add them to the mesh to create the triangles.
        $triangleIndices = [Int32Collection]::new()
        foreach ($index in $indices) { 
            $triangleIndices.Add($index)
        }

        # The mesh is formed from the triangleIndices (triangle corners).
        $this.meshGeometry.TriangleIndices = $triangleIndices
        # Add the mesh to the GeometryModel.
        $this.geometryModel.Geometry = $this.meshGeometry 
    }

    [void]AddColoredMaterial ([string]$color) {
        
        $coloredMaterial = [DiffuseMaterial]::new([SolidColorBrush]::new($color))
        $this.geometryModel.Material = $coloredMaterial
        $this.geometryModel.BackMaterial = $coloredMaterial
    }

    # As this is a general class for models, we need the texture coordinates.
    # We do not know which shape this class represents.
    [void]AddTexturedMaterial ([string]$texturePath, [PointCollection]$textureCoords) {
       
        $this.meshGeometry.textureCoordinates = $textureCoords

        $brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new($texturePath)))
        $textureMaterial = [DiffuseMaterial]::new($brush)

        $this.geometryModel.material = $textureMaterial
        $this.geometryModel.BackMaterial = $textureMaterial

    }

    # This can be used to rotate the object, or to animate it.
    # Apparently we need an animation to move the object in render time.
    [void]Rotate ([Vector3D]$axis, [double]$angle, [double]$milliseconds) {

        [DoubleAnimation]$animation = [DoubleAnimation]::new(0,$angle,[timespan]::new(0,0,0,0, $milliseconds))
        [AxisAngleRotation3D]$axisRotation = [AxisAngleRotation3D]::new($axis, 0.0)
        [RotateTransform3D]$rotation = [RotateTransform3D]::new($axisRotation, $this.GetCenterPoint())

        $this.geometryModel.Transform = $rotation
        $rotation.Rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $animation)
    }

    [void]Transform ([Vector3D]$direction, [double]$distance, [double]$milliseconds) {

        [DoubleAnimation]$animation = [DoubleAnimation]::new(0, $distance, [timespan]::new(0,0,0,0, $milliseconds))
        $transform = [TranslateTransform3D]::new($direction)

        $this.geometryModel.Transform = $transform
        $transform.Transform.BeginAnimation()

    }
    
    [void]Animate(){
        # initialize the animations for this object....
    }
}