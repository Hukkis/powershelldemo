Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5


Class Door : RectangleCube {

    [boolean]$isOpen = $false;

    # Constructor. The MeshGeometry3D and GeometryModel3D are done in the base class constructor.
    Door ([Point3D]$origin, [double]$width, [double]$height) : base($origin, $width, $height, 0.2) {

    }

    [Void]Interact($startTime) {
        
        if($this.isOpen) { $angle = 0 }
        else { $angle = -90 }
        $this.isOpen = -not $this.isOpen;

        $this.Rotate(
            [Vector3D]::new(0,1,0),
            $angle,
            50,
            $startTime
        )
    }
    
    [Void]Rotate([Vector3D]$axis, [double]$angle, [double]$duration, [double]$startTimeInMilliSeconds) {
            
        # Create the animation.
        [DoubleAnimation]$animation = [DoubleAnimation]::new(0,$angle,[timespan]::new(0,0,0,0, $duration))
        $animation.BeginTime = (New-Object TimeSpan(0,0,0,0, $startTimeInMilliSeconds))
        
        # Attach the animation to the geometrymodel.
        [AxisAngleRotation3D]$axisRotation = [AxisAngleRotation3D]::new($axis, 0.0)
        $geometry = $this.GetGeometryModel()
        [RotateTransform3D]$rotation = [RotateTransform3D]::new($axisRotation, [Point3D]::new($geometry.Bounds.X, $geometry.Bounds.Y, $geometry.Bounds.Z))
        $geometry.Transform = $rotation
        $rotation.Rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $animation)
    }    
}

