Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# Not an aeroplane ;)
Class Plane : WorldObject3D {

    # Changeable values.
    [Point3D]$p0 # Left corner.
    [Point3D]$p1
    [Point3D]$p2
    [Point3D]$p3
    [string]$color = "#FFFFFF"

    # Constructor.
    Plane ([Point3D]$p0, [Point3D]$p1, [Point3D]$p2, [Point3D]$p3) {

        $this.p0 = $p0
        $this.p1 = $p1
        $this.p2 = $p2
        $this.p3 = $p3

        $this.Draw();
    }

    Plane ([Point3D]$p0, [Point3D]$p1, [Point3D]$p2, [Point3D]$p3, [string]$color) {

        $this.p0 = $p0
        $this.p1 = $p1
        $this.p2 = $p2
        $this.p3 = $p3
        if(($color.startswith('#')) -and ($color.length -eq 7)) { $this.color = $color }
        $this.Draw();
    }

    [void]AddTexture([string]$texturePath){
    
        # From bottom left clockwise. Texture 0,0 is top left corner.
        # So the count for the texture coordinates is "behind" by one.
        $textureCoords = [PointCollection]::new()
        
        $textureCoords.Add([Point]::new(0, 1))
        $textureCoords.Add([Point]::new(0, 0))
        $textureCoords.Add([Point]::new(1, 0))
        $textureCoords.Add([Point]::new(1, 1))
       
        ([WorldObject3D]$this).AddTexturedMaterial($texturePath, $textureCoords)
    }

    [void]Draw () {

        [Point3DCollection]$vertices = [Point3DCollection]::new()

        # Define the corners of the rectangle in 3D points (vertices).
        $vertices.Add($this.p0) # Index 0
        $vertices.Add($this.p1) # Index 1
        $vertices.Add($this.p2) # Index 2
        $vertices.Add($this.p3) # Index 3
    
        # The indices of the vertices that define the triangles. The indices represent the corners of each triangle.
        # Three indices form a single triangle. In the case of the rectangle, we form two triangles.
        [int[]]$indices = @(0,2,1, 3,2,0)

        # Call the Draw() method from the parent class.
        ([WorldObject3D]$this).Draw($vertices, $indices)
        ([WorldObject3D]$this).AddColoredMaterial($this.color)
    }
}