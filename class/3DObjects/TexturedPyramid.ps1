Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5


Class TexturedPyramid : WorldObject3D {

    # Changeable values.
    [Point3D]$origin # Left corner.
    [double]$width
    [double]$height
    [string]$texturePath = $null
    

    # Constructors.
    TexturedPyramid ([Point3D]$origin, [double]$width, [double]$height) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height

        $this.Draw();
    }

    TexturedPyramid ([Point3D]$origin, [double]$width, [double]$height, [string]$texturePath) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        $this.texturePath = $texturePath

        $this.Draw();
    }

    [void]Draw () {

        ## Note ##
        # This is not the most optimized way to draw a pyramid. 
        # Better version for a simple method can be found from the Pyramid.ps1 class file.
        # This is done with multiple vertices (three per corner) in order to get the textures right for now.
        # If we would not draw the pyramid like this, we would need a proper UV map.

        # Define the vertices (points).
        $vertices = [Point3DCollection]::new()

        # Floor.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)) # Index 0, Left corner.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z + $this.width))
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z + $this.width))
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z))

        # First Face.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)) #4
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z))
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + $this.height, $this.origin.Z + ($this.width / 2)))
        
        # Left Face.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z + $this.width)) # 7
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)) 
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + $this.height, $this.origin.Z + ($this.width / 2)))
        # Back Face.
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z + $this.width)) # 10
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z + $this.width))
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + $this.height, $this.origin.Z + ($this.width / 2)))
        # Right Face.
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z)) # 13
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z + $this.width))
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + $this.height, $this.origin.Z + ($this.width / 2)))

        # The indices of the vertices that define the triangles. 
        # The indices represent the corners of each triangle.
        # Three indices form a single triangle.
        $indices = @(0,3,1, 3,2,1, 4,5,6, 7,8,9, 10,11,12, 13,14,15)

        # Call the base object Draw() method.
        ([WorldObject3D]$this).Draw($vertices, $indices)
        # Add the textured material.
        if($this.texturePath) {
            $this.AddTexturedMaterial($this.texturePath)
        }
    }

    [void]AddTexturedMaterial ([string]$texturePath) {

        # A bit more complicated version of the Pyramid textured materials.

        $brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new($texturePath)))
        $textureMaterial = [DiffuseMaterial]::new($brush)

        # First we will map the floor texture.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0,1))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0,0))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(1,0))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(1,1))

        # Then we will need all the faces of the pyramid.
        # Extract the number of vertices that the floor has, 
        # and divide by the number of vertices on the faces.
        [int]$faceCount = ($this.meshGeometry.Positions.count -4) / 3

        for([int]$i = 0; $i -lt $faceCount; $i++) {
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(0,0))
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(0,1))
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(1,0.5))
        }

        $this.geometryModel.material = $textureMaterial;
        $this.geometryModel.BackMaterial = $textureMaterial        
    }
}
