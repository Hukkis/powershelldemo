Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# This is just a wrapper class for a collection of [Tree] objects.
# Generate a randomly allocated and sized set of trees in a specified area.
Class ForestGridSystem {

    # Changeable public values.
    [Point3D]$origin # Left corner.
    [float]$areaWidth = 20
    [float]$areaDepth = 20
    [string[]]$woodTextures
    [string[]]$leafTextures

    # Private.
    [Tree[]]$private:trees
    
    # Constructors.
    ForestGridSystem ([Point3D]$origin, [float]$areaWidth, [float]$areaDepth) {

        $this.origin = $origin
        $this.areaWidth = $areaWidth;
        $this.areaDepth = $areaDepth
        $this.Draw();
    }

    ForestGridSystem ([Point3D]$origin, [float]$width, [float]$height, [string[]]$woodTexturePaths, [string[]]$leafTexturePaths) {

        $this.origin = $origin;
        $this.width = $width;
        $this.height = $height;
        $this.woodTextures = $woodTexturePaths;
        $this.leafTextures = $leafTexturePaths;

        $this.Draw();
    }

    [Void]Draw() {
       
        # We need to define a two dimensional collection, that represents the grid where the trees are set.
        # Let's say, that each hex is 1x1 in the coordinate scale.

        $forestGrid = New-Object 'object[,]' $this.areaWidth, $this.areaDepth
        
        # Initialize all the values as false to represent that they are free hexes.
        for($x = 0; $x -lt $forestGrid.GetLength(0); $x++) {
            for($y = 0; $y -lt $forestGrid.GetLength(1); $y++) {
                $forestGrid[$x,$y] = $false;
            }
        }


    }

    # This is a bit dirty.
    # We are using a input parameter here.
    [Void]AddToModelGroup ([Model3DGroup]$modelGroup) {
        $modelGroup.Children.Add($this.trunk.GetGeometryModel());
        $modelGroup.Children.Add($this.leaves.GetGeometryModel());
    } 
}

