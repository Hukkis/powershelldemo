Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# This is a wrapper class for multiple tree objects.
# It generates the trees to a given area with random attributes.
# This first iteration creates the trees in similar pattern and 
# with consistent space between them.

Class Forest {

    # Measurements.
    [Point3D]$origin
    [float]$width
    [float]$depth

    # Textures.
    [string]$trunkTexture
    [string]$leafTexture

    # Subobjects.
    [Tree[]]$private:trees

    # Draw specs.
    [float]$private:treeMinWidth = .5
    [float]$private:treeMaxWidth = 3
    [float]$private:treeMinHeight = 5
    [float]$private:treeMaxHeight = 15
    [float]$private:leafMinWidth = 3
    [float]$private:leafMaxWidth = 6

    # This will be replaced with a two dimensional grid in the future.
    # Or some other sort of system.
    [float]$private:spaceBetweenTrees = 10

    Forest([Point3D]$origin, [float]$width, [float]$depth, [string]$trunkTexture, [string]$leafTexture) {

        $this.origin = $origin;
        $this.width = $width;
        $this.depth = $depth;
        $this.trunkTexture = $trunkTexture;
        $this.leafTexture = $leafTexture;

        $this.Draw()

    }

    Draw () {

        $initialX = $this.origin.X # 0
        $initialZ = $this.origin.Z # 0

        for($x = $initialX; $x -lt ($initialX + $this.width)) {
            for($z = $initialZ; $z -lt ($initialZ + $this.depth)) {

                # Calculate random values.
                $treeHeight = Get-Random -Minimum $this.treeMinHeight -Maximum $this.treeMaxHeight
                $treeWidth = Get-Random -Minimum $this.treeMinWidth -Maximum $this.treeMaxWidth
                $leaves = Get-Random -Minimum $this.leafMinWidth -Maximum $this.leafMaxWidth
                
                # Create the tree.

                $this.trees += [Tree]::new([Point3D]::new($x, 0, $z), $treeWidth, $treeHeight, $leaves, $this.trunkTexture, $this.leafTexture );
                $z += $this.spaceBetweenTrees
            }
            $x += $this.spaceBetweenTrees
        }
    }

    # Iterate the trees, and pass the modelgroup to their AddToModelGroup method.
    [void]AddToModelGroup ([Model3DGroup]$modelGroup) {
        foreach ($tree in $this.trees) {
            $tree.AddToModelGroup($modelGroup);
        }
    }
}