Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# A simple point holder for four different points to create a rectangle.
# Can be used as a construction piece for example for the cube.
# The triangleIndices are defined automatically. 
class Rectangle3D
{
    [Point3D]$p0 # Bottom left.
    [Point3D]$p1 # Top left.
    [Point3D]$p2 # Top right.
    [Point3D]$p3 # Bottom right.
    [int[]]$indices = @(0,2,1, 3,2,0)

    # A helper function to determine the texture coordinates for the rectangle.
    # This cannot be modified, i.e. use this only if the texture does not need customization.
    static [Point[]]GetTextureCoordinates() {

        $textureCoords = @()
        $textureCoords += [Point]::new(0,1)
        $textureCoords += [Point]::new(0,0)
        $textureCoords += [Point]::new(1,0)
        $textureCoords += [Point]::new(1,1)

        return $textureCoords
    }

    Rectangle3D([Point3D]$P0, [Point3D]$P1, [Point3D]$P2, [Point3D]$P3)
    {
        $this.p0 = $P0;
        $this.p1 = $P1;
        $this.p2 = $P2;
        $this.p3 = $P3;
    }

    # Call this method if you want to add a rectangle to an existing mesh.
    [Void]AddToMesh([MeshGeometry3D]$mesh) {

        # Create a new collection for the triangleIndices (corners) and add them to the mesh.
        # Take into account the current number of vertice positions in the mesh.
        [Int32Collection]$triangleIndices = [Int32Collection]::new()
        foreach ($index in $this.indices) {
            $triangleIndices.Add(($index + $mesh.Positions.count))
        }

        $triangleIndices | foreach {
            $mesh.TriangleIndices.Add($_)
        }

        # Add the positions of the vertices (points) to the mesh.
        $mesh.Positions.Add($this.p0)
        $mesh.Positions.Add($this.p1)
        $mesh.Positions.Add($this.p2)
        $mesh.Positions.Add($this.p3)
    }
}