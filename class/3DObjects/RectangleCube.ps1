Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5


Class RectangleCube : WorldObject3D {

    # Changeable values.
    [Point3D]$origin # Left corner.
    [double]$width
    [double]$height
    [double]$depth
    $color = '#c909de'
    

    # Constructor. The MeshGeometry3D and GeometryModel3D are done in the base class constructor.
    RectangleCube ([Point3D]$origin, [double]$width, [double]$height, [double]$depth) : base() {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        $this.depth = $depth

        $this.Draw();
    }

    # Alternative constructor. The MeshGeometry3D and GeometryModel3D are done in the base class constructor.
    RectangleCube ([Point3D]$origin, [double]$width, [double]$height, [double]$depth, [string]$color) : base() {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        $this.depth = $depth
        if(($color.startswith('#')) -and ($color.length -eq 7)) { $this.color = $color }

        $this.Draw();
    }

    # This object is drawn a bit differently compared to the base object.
    # We do not need to call the base Draw() method.
    # Not the most optimized way, as we have three vertices for each corner (combine the rectangles).
    [void]Draw () {

        # Create the cube using the Rectangle3D helper class.
        # Just a concept. This can be used for more complicated purposes as well.
        [Rectangle3D[]]$rectangles = @()

        # Front.
        $rectangles += [Rectangle3D]::new(
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z),
            [Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z)
        )

         # Left.
        $rectangles += [Rectangle3D]::new(
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z),
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)
        )

        # Back
        $rectangles += [Rectangle3D]::new(
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z - $this.depth)
        )

         # Right.
        $rectangles += [Rectangle3D]::new(
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z - $this.depth)
        )

        # Top.
        $rectangles += [Rectangle3D]::new(
            [Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z),
            [Point3D]::new($this.origin.X, $this.origin.Y + $this.height, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y + $this.height, $this.origin.Z)
        )

        # Bot.
        $rectangles += [Rectangle3D]::new(
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z - $this.depth),
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z),
            [Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z - $this.depth)
        )

        foreach ($rect in $rectangles) {
            $rect.AddToMesh($this.meshGeometry)
        }
        
        # As we do not call the base Draw() method, we need to assign the Mesh Geometry here.
        $this.geometryModel.Geometry = $this.meshGeometry
        # Call the base object material function.
        ([WorldObject3D]$this).AddColoredMaterial($this.color)
    }

    [void]AddTextureOnAllFaces([string]$texturePath){

        $brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new($texturePath)))
        $textureMaterial = [DiffuseMaterial]::new($brush)

        # From bottom left clockwise. Texture 0,0 (UV -> XY) is top left corner.
        # The Origo for texturecoordinates (UV) is in the top left corner.
        # Each TextureCoordinate point corresponds to a single Vertex on the model.
        # Therefore each face needs four texturecoordinates.
        # The coordinate points stay the same regardless, we just need to tell the mesh where to place them.

        # We know that the each face of the cube is formed from four vertices (points).
        # Each corner has three vertices (the crossing recrangles).
        # Therefore we can count the number of faces with the following simple calculation.
        $faceCount = $this.meshGeometry.Positions.count / 4

        # We add the same texture coordinates for each of the faces.
        # WPF reads the texture coordinates in the order in which they are pointed into the collection.
        for($i = 0; $i -lt $faceCount; $i++) {
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(0, 1))
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(0, 0))
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(1, 0))
            $this.meshGeometry.TextureCoordinates.Add([Point]::new(1, 1))
        }

        $this.geometryModel.material = $textureMaterial
        $this.geometryModel.BackMaterial = $textureMaterial
    }

    # For this method to work, we need to have a texture sheet with a specific pattern.
    # The pattern for a cube texture is:
    #         *Top*
    # *left* *Front* *right* *back*
    #         *Bot*
    [void]AddMappedTexture([string]$texturePath) {

        $brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new($texturePath)))
        $textureMaterial = [DiffuseMaterial]::new($brush)

        # As the cube has 6 faces, we need 24 texture coordinates.
       
        # Front face.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.25, .66))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.25, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.5, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.5, .667))

        # Left face.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0, .667))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.25, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.25, .667))

        # Back face.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.75, .667))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.75, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(1, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(1, .667))

        # Right face.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.5, .667))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.5, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.75, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.75, .667))

        # Top.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.25, .334))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.25, 0))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.5, 0))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.5, .334))

        # Bot.
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.25, 1))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.25, .667))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.5, .667))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(.5, 1))

        $this.geometryModel.material = $textureMaterial
        $this.geometryModel.BackMaterial = $textureMaterial
    }
}

