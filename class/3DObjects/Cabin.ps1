Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# This is just a wrapper class for a simple cabin object, which is built using different shapes.
# This could be drawn as a single mesh, except WPF does not support multiple texture coordinates for one mesh.
# For that reason, this object is combined using multiple meshes.
# Although, one could make a texture map for the whole thing...

Class Cabin {

    ## Changeable values.
    # General.
    [Point3D]$origin # Left corner.
    [double]$width = 20
    [double]$height = 10
    [double]$depth = 20
    [double]$wallThickness = 0.2

    # Door.
    [double]$doorWidth = 3
    [double]$doorHeight = 5
    [double]$doorThickness = 0.4

    # Textures.
    [string]$wallTexture = ""
    [string]$floorTexture = ""
    [string]$roofTexture = ""
    [string]$doorTexture = ""

    # Subobjects.
    [RectangleCube]$private:leftWall
    [RectangleCube]$private:rightWall
    [RectangleCube]$private:backWall
    [WorldObject3D]$private:frontWall
    [RectangleCube]$private:floor
    [Door]$private:door
    [TexturedPyramid]$private:roof

    $color = '#c909de'

    # Constructors.
    # As this is a simple wrapper class, we will "draw" the object here.
    Cabin () {
        $this.Draw();
    }
    
    Cabin ([Point3D]$origin, [float]$width, [float]$height, [float]$depth) {

        $this.origin = $origin;
        $this.width = $width;
        $this.height = $height;
        $this.depth = $depth;

        $this.Draw();
    }

    # Constructor with textures.
    Cabin ([Point3D]$origin, [float]$width, [float]$height, [float]$depth, [string]$wallTexturePath, [string]$floorTexturePath, [string]$roofTexturePath, [string]$doorTexturePath) {

        $this.origin = $origin;
        $this.width = $width;
        $this.height = $height;
        $this.depth = $depth;

        $this.wallTexture = $wallTexturePath
        $this.roofTexture = $roofTexturePath
        $this.floorTexture = $floorTexturePath
        $this.doorTexture = $doorTexturePath

        $this.Draw();
        $this.AddTextures($this.wallTexture, $this.floorTexture, $this.roofTexture, $this.doorTexture);
    }

    [Void]Draw() {
        # Build the walls, floor and roof.
        # Rectangle constructor parameters for reference: ([Point3D]$origin, [double]$width, [double]$height, [double]$depth) 
        $this.leftWall = [RectangleCube]::new([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z), $this.wallThickness, $this.height, $this.depth);
        $this.rightWall = [RectangleCube]::new([Point3D]::new(($this.origin.X + $this.width), $this.origin.Y, $this.origin.Z), $this.wallThickness, $this.height, $this.depth)
        $this.backWall = [RectangleCube]::new([Point3D]::new($this.origin.X, $this.origin.Y, ($this.origin.Z - $this.depth)), $this.width, $this.height, $this.wallThickness)
        $this.floor = [RectangleCube]::new([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z), $this.width, $this.wallThickness , $this.depth)
        $this.door = [Door]::new(
            [Point3D]::new(
                $this.origin.X + (($this.width / 2) - ($this.doorWidth / 2)),
                $this.origin.Y,
                $this.origin.Z
            ),
            $this.doorWidth,
            $this.doorHeight
        )

        $this.roof = [TexturedPyramid]::new(
            [Point3D]::new($this.origin.X, $this.height, ($this.origin.Z - $this.depth)),
            $this.width,
            5
        )

        # Draw the front wall separately.
        $this.DrawFrontWall()
    }

    # This is a separate method, as it's easier to keep track of it as it's a custom shape.
    [void]DrawFrontWall () {

        $this.frontWall = [WorldObject3D]::new()

        # The front wall is drawn out of three rectangle cubes (to leave the door hole open)
        [RectangleCube[]]$subMeshes = @()

        # Left part.
        $subMeshes += [RectangleCube]::new(
            $this.origin,
            ($this.width - $this.doorWidth) / 2,
            $this.height,
            $this.wallThickness
        )

        # Top Part.
        $topPart = [RectangleCube]::new(
            [Point3D]::new(
                $this.origin.X + (($this.width - $this.doorWidth) / 2),
                $this.doorHeight,
                $this.origin.Z
            ),
            $this.doorWidth,
            $this.height - $this.doorHeight,
            $this.wallThickness
        )

        $subMeshes += $topPart

        # Right part. 
        $subMeshes += [RectangleCube]::new(
            [Point3D]::new(
                $this.origin.X + (($this.width / 2) + ($this.doorWidth / 2)),
                $this.origin.Y,
                $this.origin.Z
            ),
            ($this.width - $this.doorWidth) / 2,
            $this.height,
            $this.wallThickness
        )

        foreach ($mesh in $subMeshes) {
            $mesh.AddTextureOnAllFaces($this.wallTexture)
            $this.frontWall.CombineMesh($mesh.meshGeometry)
        }
        
        $this.frontWall.GeometryModel.Geometry = $this.frontWall.meshGeometry
        
        $this.frontWall.AddTexturedMaterial($this.wallTexture, $this.frontWall.meshGeometry.textureCoordinates)
    }

    [Void]AddTextures([string]$wallTexturePath, [string]$floorTexturePath, [string]$roofTexturePath, [string]$doorTexturePath) {
        $this.AddWallTexture($wallTexturePath);
        $this.AddFloorTexutre($floorTexturePath);
        $this.AddDoorTexture($doorTexturePath);
        $this.AddRoofTexture($roofTexturePath);
    }

    [Void]AddWallTexture([string]$wallTexturePath) {
        $this.leftWall.AddTextureOnAllFaces($wallTexturePath);
        $this.rightWall.AddTextureOnAllFaces($wallTexturePath);
        $this.backWall.AddTextureOnAllFaces($wallTexturePath);
    }

    [Void]AddFloorTexutre([string]$floorTexturePath) {
        $this.floor.AddTextureOnAllFaces($floorTexturePath);
    }
    
    [void]AddDoorTexture([string]$doorTexturePath) {
        $this.door.AddTextureOnAllFaces($doorTexturePath)
    }

    [Void]AddRoofTexture([string]$roofTexturePath) {
        $this.roof.AddTexturedMaterial($roofTexturePath);
    }


    # This is a bit dirty.
    # We are using a input parameter here.
    # For a better solution, check the LightPost.ps1 class.
    [Void]AddToModelGroup ([Model3DGroup]$modelGroup) {
        $modelGroup.Children.Add($this.leftWall.GetGeometryModel());
        $modelGroup.Children.Add($this.rightWall.GetGeometryModel());
        $modelGroup.Children.Add($this.backWall.GetGeometryModel());
        $modelGroup.Children.Add($this.frontWall.GetGeometryModel());
        $modelGroup.Children.Add($this.floor.GetGeometryModel());
        $modelGroup.Children.Add($this.door.GetGeometryModel());
        $modelGroup.Children.Add($this.roof.GetGeometryModel());
    } 

    [void]OpenDoor ($startTime) {
        $this.Door.Interact($startTime);
    }

}

