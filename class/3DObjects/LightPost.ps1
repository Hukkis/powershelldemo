Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# This is basically just a wrapper class for two cylinders, a cube and a light.
# It is more convenient to create shapes this way.
# They are merged into a single mesh as they contain just one texture.

Class LightPost : WorldObject3D {

    ## Properties.
    # General.
    [Point3D]$origin
    [float]$thickness = .2
    [float]$height = 15
    [int]$resolution = 20
    [string]$color = '#b0b0b0'
    # Horizontal post.
    [float]$horizontalLength = 4.7
    # Light emitting box.
    [float]$lightBoxDepth = 2.5
    [float]$lightBoxWidth = 1.4
    [float]$lightBoxHeight = .4
    # Light.
    [string]$lightColor = 'yellow'
    [float]$lightRange = 30

    # Sub objects.
    [Cylinder]$private:post
    [Cylinder]$private:horizontalPost
    [RectangleCube]$private:box
    [SpotLight]$private:spotLight

    LightPost([Point3D]$origin) {
        $this.origin = $origin; 
        $this.Draw();
    }

    [Model3D]GetLightObject () {
        return $this.spotLight;
    }

    # Draw the sub objects.
    # The actual meshGeometry is combined calling a method from a base class.
    # This object is drawn a bit differently, so the base Draw() method is not called.
    [void]Draw() {

        $this.post = [Cylinder]::new(
            [Point3D]::new($this.Origin.X, $this.Origin.Y, $this.Origin.Z),
            [Vector3D]::new(0, $this.height, 0),
            $this.thickness,
            $this.resolution
        )

        $this.horizontalPost = [Cylinder]::new(
            [Point3D]::new(
                $this.Origin.X,
                $this.height,
                ($this.Origin.Z  - $this.thickness)
            ),
            [Vector3D]::new(0,0,$this.horizontalLength),
            $this.thickness,
            $this.resolution
        )

        # The light emitting "box".
        # Bunch of calculations are done to get it positioned correctly.
        $this.box = [RectangleCube]::new(
            [Point3D]::new(
                ($this.Origin.X - ($this.lightBoxWidth / 2)),
                (($this.Origin.Y + $this.height) - $this.thickness),
                ($this.origin.Z + ($this.horizontalLength- $this.thickness) + $this.lightBoxDepth)
            ),
            $this.lightBoxWidth,
            $this.lightBoxHeight,
            $this.lightBoxDepth,
            $this.color
        )

        # Light.
        $this.spotLight = [SpotLight]::new(
            $this.lightColor,
            [Point3D]::new(
                $this.Origin.X,
                (($this.origin.Y + $this.height) - $this.lightBoxHeight),
                ($this.origin.Z + $this.horizontalLength + ($this.lightBoxDepth / 2))
            ),
            [Vector3D]::new(0,-1,0),
            90,
            90
        )

        ([WorldObject3D]$this).CombineMesh($this.post.meshGeometry);
        ([WorldObject3D]$this).CombineMesh($this.horizontalPost.meshGeometry);
        ([WorldObject3D]$this).CombineMesh($this.box.meshGeometry);
  
        $this.geometryModel.Geometry = $this.meshGeometry
        ([WorldObject3D]$this).AddColoredMaterial($this.color)  
    }
}