Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5


Class Pyramid : WorldObject3D {

    # Changeable values.
    [Point3D]$origin # Left corner.
    [double]$width
    [double]$height
    $color = '#c909de'
    

    # Constructor.
    Pyramid ([Point3D]$origin, [double]$width, [double]$height) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height

        $this.Draw();
    }

    Pyramid ([Point3D]$origin, [double]$width, [double]$height, [string]$color) {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        if(($color.startswith('#')) -and ($color.length -eq 7)) { $this.color = $color }

        $this.Draw();
    }

    [void]Draw () {

        # Define the vertices (points).
        $vertices = [Point3DCollection]::new()
        # Floor.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z)) # Index 0, Left corner.
        $vertices.Add([Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z + $this.width))
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z + $this.width))
        $vertices.Add([Point3D]::new($this.origin.X + $this.width, $this.origin.Y, $this.origin.Z))
        # Top point.
        $vertices.Add([Point3D]::new($this.origin.X + ($this.width / 2), $this.origin.Y + $this.height, $this.origin.Z + ($this.width / 2)))

        # The indices of the vertices that define the triangles. 
        # The indices represent the corners of each triangle.
        # Three indices form a single triangle.
        $indices = @(0,3,1, 3,2,1, 0,3,4, 3,2,4, 2,1,4, 1,0,4)

        # Call the base object Draw() method.
        ([WorldObject3D]$this).Draw($vertices, $indices)
        # Add the color using the base object method.
        ([WorldObject3D]$this).AddColoredMaterial($this.color)
    }

    [void]AddTexturedMaterial ([string]$texturePath) {

        $brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new($texturePath)))
        $textureMaterial = [DiffuseMaterial]::new($brush)

        ## Note to self ##
        # The problem with a pyramid with only five vertices is, that the texture will be displayed 
        # differently on different faces.
        # Depending on the face and the coordinates given to it, the texture might be 
        # "the wrong way around".
        # Therefore we can only use a simple seamless texture here.
        # If we want to make the pyramid with correct textures, we would need more vertices.

        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0,1))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0,0))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(1,0))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(1,1))
        $this.meshGeometry.TextureCoordinates.Add([Point]::new(0.5,0.5))

        $this.geometryModel.material = $textureMaterial;
        $this.geometryModel.BackMaterial = $textureMaterial        
    }
}
