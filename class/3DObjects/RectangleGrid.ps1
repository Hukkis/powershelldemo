Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# A general rectangle formed from four Vertices.
# This is used as a base structure for the basic models.
Class RectangleGrid : WorldObject3D {

    # Changeable values.
    [Point3D]$origin # Left corner.
    [double]$width
    [double]$height
    [int]$horizontalSegments
    [int]$verticalSegments
    [string]$color = "#234234"

    # Constructor.
    RectangleGrid ([Point3D]$origin, [double]$width, [double]$height, [int]$horizontalSegments, [int]$verticalSegments) : base() {

        $this.origin = $origin
        $this.width = $width
        $this.height = $height
        $this.horizontalSegments = $horizontalSegments
        $this.verticalSegments = $verticalSegments

        $this.Draw();
    }

    [void]Draw () {

        $rectangleWidth = $this.width / $this.horizontalSegments
        $rectangleHeight = $this.height / $this.verticalSegments
        [Rectangle3D[]]$rectangles = @() 

        # Go through all the single rectangles in the grid. 
        # Start drawing from the bottom left corner, and draw row by row.
        # i = "row".
        # j = "column".
        for ($i = 0; $i -lt $this.verticalSegments; $i++) {
            for ($j = 0; $j -lt $this.horizontalSegments; $j++) {
                
                $rectangles += [Rectangle3D]::new(
                    [Point3D]::new($this.origin.X + ($j * $rectangleWidth), $this.origin.Y, $this.origin.Z - ($i * $rectangleHeight)),
                    [Point3D]::new($this.origin.X + ($j * $rectangleWidth), $this.origin.Y, $this.origin.Z - (($i * $rectangleHeight) + $rectangleHeight)),
                    [Point3D]::new($this.origin.X + (($j * $rectangleWidth) + $rectangleWidth), $this.origin.Y, $this.origin.Z  - (($i * $rectangleHeight) + $rectangleHeight)),
                    [Point3D]::new($this.origin.X + (($j * $rectangleWidth) + $rectangleWidth), $this.origin.Y, $this.origin.Z - ($i * $rectangleHeight))
                )
            }
        }

        foreach ($rect in $rectangles) {
            $rect.AddToMesh($this.meshGeometry)
        }

        $this.geometryModel.Geometry = $this.meshGeometry
        ([WorldObject3D]$this).AddColoredMaterial($this.color)
    }

    [void]AddTexture([string]$texturePath){

            $brush = [ImageBrush]::new([System.Windows.Media.Imaging.BitmapImage]::new([Uri]::new($texturePath)))
            $textureMaterial = [DiffuseMaterial]::new($brush)
    
            # From bottom left clockwise. Texture 0,0 is top left corner.

            # Each rectangle is formed from four vertices.
            # Therefore we can count the number of rectangle with the following simple calculation.
            $rectCount = $this.meshGeometry.Positions.count / 4

           for($i = 0; $i -lt $rectCount; $i++) {
                $coords = [Rectangle3D]::GetTextureCoordinates()
                $coords | foreach {
                    $this.meshGeometry.TextureCoordinates.Add($_)
                }
            }

            $this.geometryModel.material = $textureMaterial
            $this.geometryModel.BackMaterial = $textureMaterial
    }

}