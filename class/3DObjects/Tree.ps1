Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# This is just a wrapper class for a simple tree object, which is built using different shapes.
# This could be drawn as a single mesh, except WPF does not support multiple texture coordinates for one mesh.
# For that reason, this object is combined using multiple meshes.
# Although, one could make a texture map for the whole thing.

Class Tree {

    # Changeable values.
    [Point3D]$origin # Left corner.
    [float]$width = 2
    [float]$height = 10
    [float]$leafWidth = 6
    [string]$woodTexture = ""
    [string]$leafTexture = ""

    # Subobjects.
    [RectangleCube]$private:trunk
    [RectangleCube]$private:leaves

    $color = '#c909de'
    
    # Constructors.
    Tree ([Point3D]$origin, [float]$width, [float]$height, [float]$leafWidth) {

        $this.origin = $origin;
        $this.width = $width;
        $this.height = $height;
        $this.leafWidth = $leafWidth;

        $this.Draw();
    }

    Tree ([Point3D]$origin, [float]$width, [float]$height, [float]$leafWidth, [string]$woodTexturePath, [string]$leafTexturePath) {

        $this.origin = $origin;
        $this.width = $width;
        $this.height = $height;
        $this.leafWidth = $leafWidth;
        $this.woodTexture = $woodTexturePath;
        $this.leafTexture = $leafTexturePath;

        $this.Draw();
        $this.AddTextures($this.woodTexture, $this.leafTexture);
    }

    [Void]Draw() {
        # Create the trunk and the leaves (a cube for now...).
        # Rectangle constructor parameters for reference: ([Point3D]$origin, [double]$width, [double]$height, [double]$depth) 
        $this.trunk = [RectangleCube]::new(
            [Point3D]::new($this.origin.X, $this.origin.Y, $this.origin.Z),
            $this.width, 
            $this.height, 
            $this.width
        );
       
        $this.leaves = [RectangleCube]::new(
            [Point3D]::new(
                $this.origin.X - (($this.leafWidth - $this.width) / 2),
                $this.origin.Y + $this.height,
                $this.origin.Z + (($this.leafWidth - $this.width) / 2)
            ),
            $this.leafWidth, 
            $this.leafWidth, 
            $this.leafWidth
        );
       
    }

    [Void]AddTextures([string]$woodTexturePath, [string]$leaftexturePath) {
        $this.trunk.AddTextureOnAllFaces($woodTexturePath);
        $this.leaves.AddTextureOnAllFaces($leaftexturePath);
    }

    # This is a bit dirty.
    # We are using a input parameter here.
    [Void]AddToModelGroup ([Model3DGroup]$modelGroup) {
        $modelGroup.Children.Add($this.trunk.GetGeometryModel());
        $modelGroup.Children.Add($this.leaves.GetGeometryModel());
    } 

    # Placeholder method.
    [Void]Rotate () {
            # $treetop1.Rotate(
    #     [Vector3D]::new(0,1,0),
    #     450,
    #     30000
    # )
    }

}

