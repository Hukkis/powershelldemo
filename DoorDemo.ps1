Using Assembly PresentationCore
Using Assembly PresentationFramework
Using Namespace System.Windows
Using Namespace System.Windows.Markup
using Namespace System.Windows.Controls
using Namespace System.Windows.Media
using Namespace System.Windows.Media.Media3D
Using Namespace System.Windows.Media.Animation
Using Namespace System.Windows.Input
Using Namespace System.Windows.PropertyPath
Using Namespace System.Windows.DependencyProperty
#REQUIRES -version 5

# Include classes.
. .\class\WorldObject3D.ps1
. .\class\cube.ps1
. .\class\Pyramid.ps1
. .\class\Plane.ps1
. .\class\Rectangle3D.ps1
. .\class\RectangleCube.ps1
. .\class\RectangleGrid.ps1
. .\class\Cabin.ps1
. .\class\Door.ps1

# Notes:
# Window has viewport
# Viewport has a camera and a ModelVisual
# ModelVisual has a Model3DGroup or a Model
# Model3DGroup has lights and models (GeometryModel3D)
# GeometryModel3D has Geometry (MeshGeometry3D)
# MeshGeometry3D has TriangleIndices & Positions (3DPoints)


# In PowerShell there is no good way to create this without using the XML syntax with herestring.
function Set-Window {

    [system.windows.Window] $window = [System.Windows.Markup.XamlReader]::Parse(@'
    <Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="Vararehtorin aamulenkki"
    x:Name="MainWindow"
    Height="1024" Width="1024">
    <Grid>
        <Viewport3D Grid.Row="0" Grid.Column="0" Name="MainViewport"/>
    </Grid>
    </Window>
'@)
    return $window
}

function Set-Camera {

    # If we look at the scene from +z coordinates towards the origo, we get the following directions (right hand coordinates):
    # +X == 'right'
    # +Y == 'up'
    # +Z == 'towards the viewer'
    # This setup is used when determining the scene. Other than that, it does not matter.

    $PerspectiveCamera = [PerspectiveCamera]::new()
    $PerspectiveCamera.FieldOfView = 90
    $PerspectiveCamera.Position = [Point3D]::new(0,5,12)
    $PerspectiveCamera.LookDirection = [Vector3D]::new(0,-5,-12)
    $PerspectiveCamera.UpDirection = [Vector3D]::new(0.0, 1.0, 0.0)

    return $perspectiveCamera
}

# Setting up the lights.
# Adds the light effects to the given Model3DGroup.
Function Set-Lights {
    Param($lightgroup)

    # Singular ambient light, that illuminates the whole scene.
    # $lightgroup.Children.Add([AmbientLight]::new('white'))

    # Directional lights, that illuminate towards the given vector.
    # To illuminate most of the scene, we need two of these.
    $lightgroup.Children.Add([DirectionalLight]::new('white', [Vector3D]::new(-1,-1,-1)))
    $lightgroup.Children.Add([DirectionalLight]::new('white', [Vector3D]::new(1,1,-1)))

    # Light, that illuminates only the given area.
    # This configuration works as a streetlight for example.
    # $pointLight = [PointLight]::new('orange', [Vector3D]::new(1,5,25))
    # $pointLight.Range = 15;
    # $pointLight.ConstantAttenuation = 1.5;
    # $lightgroup.Children.Add($pointLight)
}

Function Move-Camera {
    Param([Vector3D]$direction, [Vector3D]$lookDirection)

    if($direction) {
        $startPosition = $Global:Camera.Position
        [Point3D]$endPosition = [Point3D]::new(0,0,0)
        $endPosition = [Point3D]::Add($startPosition,$direction)
        $Global:Camera.Position = $endPosition 
    }
    
    if($lookDirection){
        $startLookAt = $Global:Camera.LookDirection
        [Vector3D]$lookAt = [Vector3D]::new(0,0,0)
        $lookAt = $startLookAt + $lookDirection
        $Global:Camera.LookDirection = $lookAt
    }
}

# Just a temporary wrapper for the cabin.
# Later on this will be a class.
function Create-Doors { 

    [object[]]$doors = @()

    $doorCount = 63;
    $xCoord = -90;

    for($i = 0; $i -lt $doorCount; $i++ ) {

        $cube = [Door]::new([Point3D]::new($xCoord, 0, 0), 2, 4);
        $cube.AddTextureOnAllFaces("$texturePath\plank.jpg")
        $doors += $cube
        $xCoord += 3;
    }

    return $doors
}

function Rotate-Doors {
    Param(
        [RectangleCube[]]$doors,
        [Vector3D]$axis,
        [double]$angle, 
        [double]$duration,
        [double]$startTimeInMilliSeconds
    )

    # Create the animation.
    [DoubleAnimation]$animation = [DoubleAnimation]::new(0,$angle,[timespan]::new(0,0,0,0, $duration))
    $animation.BeginTime = (New-Object TimeSpan(0,0,0,0,$startTimeInMilliSeconds))
    [AxisAngleRotation3D]$axisRotation = [AxisAngleRotation3D]::new($axis, 0.0)

    $doors | foreach {
        # Attach the animation to the geometryModel.
        $geometry = $_.GetGeometryModel()
        [RotateTransform3D]$rotation = [RotateTransform3D]::new($axisRotation, [Point3D]::new($geometry.Bounds.X, $geometry.Bounds.Y, $geometry.Bounds.Z))
        $geometry.Transform = $rotation
        $rotation.Rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $animation)
    } 
}

function Rotate-Door {
    Param(
        [RectangleCube]$door,
        [Vector3D]$axis,
        [double]$angle, 
        [double]$duration,
        [double]$startTimeInMilliSeconds
    )

    # Create the animation.
    [DoubleAnimation]$animation = [DoubleAnimation]::new(0,$angle,[timespan]::new(0,0,0,0, $duration))
    $animation.BeginTime = (New-Object TimeSpan(0,0,0,0,$startTimeInMilliSeconds))
    [AxisAngleRotation3D]$axisRotation = [AxisAngleRotation3D]::new($axis, 0.0)
        
    # Attach the animation to the geometryModel.
    $geometry = $door.GetGeometryModel()
    [RotateTransform3D]$rotation = [RotateTransform3D]::new($axisRotation, [Point3D]::new($geometry.Bounds.X, $geometry.Bounds.Y, $geometry.Bounds.Z))
    $geometry.Transform = $rotation
    $rotation.Rotation.BeginAnimation([AxisAngleRotation3D]::AngleProperty, $animation)
}

function Start-MorningJog {
    Param([RectangleCube[]]$doors)

    # Time in milliseconds.
    $timeBetweenLoops = 1000
    $numberOfLoops = 30
    $step = 1

  # Rotate-Doors -doors $doors -axis ([Vector3D]::new(0,1,0)) -angle 480 -duration 30000 -startTimeInMilliSeconds 0

    for([int]$j = 0; $j -lt $numberOfLoops; $j++) {
        for([int]$k = $step; $k -lt ($doors.count + $step); $k += $step) {
            if($doors[$k -1]) { $doors[$k-1].Interact($timeBetweenLoops) }
        }
        $step++
    }
}

# Initialize the scene.
# Call the functions that create the window, camera, models etc.
# Render the scene.
function Initialize-Scene {

    # Properties.
    # Add one Model3DGroup to work as a "base container", aka the world.
    # Add different Model3DGroups to that base container. 
    $modelVisual = [ModelVisual3D]::new()
    $lightVisual = [ModelVisual3D]::new()
    $doorVisual = [ModelVisual3D]::new()
    $lightGroup = [Model3DGroup]::new()
    $modelGroup = [Model3DGroup]::new()
    $doorGroup = [Model3DGroup]::new()

    $texturePath = Join-Path -Path $PSScriptRoot -childPath .\textures

    # Construct the scene.  
    # Create the ground grid.
    $ground = [RectangleGrid]::new(
        [Point3D]::new(-100,0,60),
        200,
        80,
        10,
        10
    )
    $ground.AddTexture("$texturePath\concrete.jpg")
    $modelGroup.Children.add($ground.GetGeometryModel())

    # Create the sky background
    $sky = [Plane]::new(
        [Point3D]::new(-100,-0.1,-20),
        [Point3D]::new(-100,50,-20),
        [Point3D]::new(100,50,-20),
        [Point3D]::new(100,-0.1,-20)
    )
    $sky.AddTexture("$texturePath\concrete.jpg")
    $modelGroup.Children.add($sky.GetGeometryModel())

    $leftSky = [Plane]::new(
        [Point3D]::new(-100,-0.1,60),
        [Point3D]::new(-100,50,60),
        [Point3D]::new(-100,50,-20),
        [Point3D]::new(-100,-0.1,-20)
    )
    $leftsky.AddTexture("$texturePath\concrete.jpg")
    $modelGroup.Children.add($leftsky.GetGeometryModel())

    $rightSky = [Plane]::new(
        [Point3D]::new(100,-0.1,60),
        [Point3D]::new(100,50,60),
        [Point3D]::new(100,50,-20),
        [Point3D]::new(100,-0.1,-20)
    )
    $rightsky.AddTexture("$texturePath\concrete.jpg")
    $modelGroup.Children.add($rightsky.GetGeometryModel())

    [RectangleCube[]]$doors = Create-Doors
    $doors | foreach {
        $doorGroup.Children.Add($_.GetGeometryModel())
    }
  
    Set-Lights -lightgroup $lightGroup
    $mainWindow = Set-Window
    $Global:Camera = Set-Camera

    Start-MorningJog -doors $doors

    # Run this when the window is loaded.
    $mainWindow.Add_Loaded({
        $mainWindow.Content.Background = 'white'
        $MainViewPort = $mainWindow.FindName('MainViewport')
        $MainViewport.Camera = $camera
        $modelVisual.Content = $modelGroup
        $lightVisual.Content = $lightGroup
        $doorVisual.Content = $doorGroup
        $MainViewport.Children.Add($modelVisual)
        $MainViewPort.Children.Add($lightVisual)
        $MainViewPort.Children.Add($doorVisual)
       
    })

    $mainWindow.ShowDialog() | Out-Null
}

Initialize-Scene


# Camera controls.
# Very poorly executed, but works for debugging purposes.
# If we want a better camera, this needs a rewrite.
# The end result will most likely not have any user interraction though.
[System.Windows.EventManager]::RegisterClassHandler([system.windows.Window], [Keyboard]::KeyDownEvent , [KeyEventHandler] {
    Param ([Object] $sender, [System.Windows.Input.KeyEventArgs]$eventArgs)

    # Define the default vector.
    [Vector3D]$direction = [Vector3D]::new(0,0,0)
    [Vector3D]$lookDirection = [Vector3D]::new(0,0,0)
    $move = $false;
    $rotate = $false;

    Switch ($eventArgs.key){
        'w'{
            $direction.Z = -1
            $move = $true;
           Break;
        }
        's'{
            $direction.Z = 1
            $move = $true;
            Break;
        }
        'a'{
            $direction.X = -1
            $move = $true;
            Break;
        }
        'd'{
            $direction.X = 1
            $move = $true;
            Break;
        }
        'e'{
            $direction.Y = 1
            $move = $true;
            Break;
        }
        'q'{
            $direction.Y = -1
            $move = $true;
            Break;
        }
        'Up'{
            $lookDirection.Y = 1
            $rotate = $true;
            Write-host "löytys"
            Break;
        }
        'Down'{
            $lookDirection.Y = -1
            $rotate = $true;
            Break;
        }
        'Left'{
            $lookDirection.X = -1
            $rotate = $true;
            Break;
        }
        'Right'{
            $lookDirection.X = 1
            $rotate = $true;
            Break;
        }
        default{
            break;
        }
    }
    if($move) { Move-Camera -direction $direction }
    if($rotate) { Move-Camera -lookDirection $lookDirection }
    
})
